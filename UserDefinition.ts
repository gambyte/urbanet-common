export default class UserDefinition {
    public Email: string;
    public Phone: string;
    public AlternativePhone: string;
    public Password: string;
    public FirstName: string;
    public LastName: string;

    constructor() {
        this.Email = "";
        this.Password = "";
        this.Phone = "";
        this.AlternativePhone = "";
        this.FirstName = "";
        this.LastName = "";
    }
}