export const isEmpty = (text) => {
    return text === ""
};

export const minLength = (text, minLength) => {
    return text.length >= minLength
}

export const maxLength = (text, maxLength) => {
    return text.length <= maxLength
};
